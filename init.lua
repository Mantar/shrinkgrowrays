-- MTG check
local use_mtg = minetest.get_modpath("default")

-- MineClone2 check
local use_mcl = minetest.get_modpath("mcl_core")

function shallow_copy(t)
   local t2 = {}
   for k,v in pairs(t) do
      t2[k] = v
   end
   return t2
end

--the growth ray
minetest.register_craftitem("shrinkgrowrays:growth_ray", {
       description = "Growth Ray",
       inventory_image = "open_ai_growth_ray.png",

       on_use = function(itemstack, user, pointed_thing)
	  local pos = user:getpos()
	  pos.y = pos.y + 1.25
	  local dir = user:get_look_dir()

	  dir.x = dir.x * 15
	  dir.y = dir.y * 15
	  dir.z = dir.z * 15

	  local object = minetest.add_entity(pos, "shrinkgrowrays:growth_ray_ray")

	  object:setvelocity(dir)
       end,
})

local function get_originals(luaentity, objprop)

   if not luaentity.visual_size then
      luaentity.visual_size = shallow_copy(objprop.visual_size)
      luaentity.collisionbox = shallow_copy(objprop.collisionbox)
   end
   return luaentity.visual_size, luaentity.collisionbox
end

local blacklist = {
   ["shrinkgrowrays:growth_ray_ray"] = true,
   ["shrinkgrowrays:shrink_ray_ray"] = true,
   ["mcl_wieldview:wieldview"] = true,
}

local function alter_size(object, bigger)
   local pos = object:getpos();
   local luaent = object:get_luaentity()
   if not luaent then return false end -- something's screwy, abort
   if blacklist[luaent.name] then return false end
   local oprop = object:get_properties()
   local orig_size, orig_colbox = get_originals(luaent, oprop)
   local size = oprop.visual_size
   local colbox = oprop.collisionbox

   for _, i in pairs({"x", "y", "z"}) do
      size[i] = size[i] * (bigger and 2 or 0.5)
   end
   if size.x > orig_size.x * 4  or size.x < orig_size.x / 2 then
      return false -- out of bounds, no effect
   end
   if size.x == orig_size.x then
      colbox = orig_colbox
      size = orig_size
   else
      for i = 1,table.getn(colbox) do
	 colbox[i] = colbox[i] * (bigger and 2 or 0.5)
      end
      pos.y = pos.y + colbox[2]
   end
   if bigger then object:setpos(pos) end
   object:set_properties({ visual_size = size, collisionbox = colbox })
   return true
end

--the growth ray orb
minetest.register_entity("shrinkgrowrays:growth_ray_ray", {
    visual = "sprite",
    physical = true,
    collide_with_objects = false,
    textures = {"open_ai_growth_ray_ray.png"},

    on_activate = function(self, staticdata, dtime_s)
       self.object:set_armor_groups({immortal = 1})
    end,

    on_step = function(self,dtime)
       local pos = self.object:getpos()
       local vel = self.object:getvelocity()

       for _,object in ipairs(minetest.env:get_objects_inside_radius(pos, 2)) do
	  --only collide with other mobs and players

	  if alter_size(object, true) then
	     self.object:remove()
	  end
       end

       if ( self.oldvel and ((self.oldvel.x ~= 0 and vel.x == 0)
	       or (self.oldvel.y ~= 0 and vel.y == 0)
	       or (self.oldvel.z ~= 0 and vel.z == 0)) ) then
	  self.object:setvelocity({x=0,y=0,z=0})
	  self.object:remove()
       end

       self.oldvel = vel
    end,
})



--the shrink ray
minetest.register_craftitem("shrinkgrowrays:shrink_ray", {
       description = "Shrink Ray",
       inventory_image = "open_ai_shrink_ray.png",

       on_use = function(itemstack, user, pointed_thing)
	  local pos = user:getpos()
	  pos.y = pos.y + 1.25
	  local dir = user:get_look_dir()

	  dir.x = dir.x * 15
	  dir.y = dir.y * 15
	  dir.z = dir.z * 15

	  local object = minetest.add_entity(pos, "shrinkgrowrays:shrink_ray_ray")

	  object:setvelocity(dir)
       end,
})

--the shrink ray orb
minetest.register_entity("shrinkgrowrays:shrink_ray_ray", {
    visual = "sprite",
    physical = true,
    collide_with_objects = false,
    textures = {"open_ai_shrink_ray_ray.png"},

    on_activate = function(self, staticdata, dtime_s)
       self.object:set_armor_groups({immortal = 1})
    end,

    on_step = function(self,dtime)
       local pos = self.object:getpos()
       local vel = self.object:getvelocity()

       for _,object in ipairs(minetest.env:get_objects_inside_radius(pos, 2)) do
	  --only collide with other mobs and players

	  --add exception if a nil entity exists around it
	  if not object:is_player() and (object:get_luaentity() and object ~= self.object) then
	     if alter_size(object, false) then
		self.object:remove()
	     end
	  end
       end

       if ( self.oldvel and ((self.oldvel.x ~= 0 and vel.x == 0)
	       or (self.oldvel.y ~= 0 and vel.y == 0)
	       or (self.oldvel.z ~= 0 and vel.z == 0)) ) then
	  self.object:setvelocity({x=0,y=0,z=0})
	  self.object:remove()
       end

       self.oldvel = vel
    end,
})

if use_mtg then
   minetest.register_craft({
	 output = "shrinkgrowrays:shrink_ray",
	 recipe = {
	    {"default:steel_ingot", "default:mese_crystal", "default:diamond"},
	    {"default:steel_ingot", "",  ""}
	 }
   })

   minetest.register_craft({
	 output = "shrinkgrowrays:growth_ray",
	 recipe = {
	    {"default:steel_ingot", "default:diamond", "default:mese_crystal"},
	    {"default:steel_ingot", "",  ""}
	 }
   })
elseif use_mcl then
   minetest.register_craft({
	 output = "shrinkgrowrays:shrink_ray",
	 recipe = {
	    {"mcl_core:iron_ingot", "mesecons:redstone", "mcl_core:diamond"},
	    {"mcl_core:iron_ingot", "",  ""}
	 }
   })

   minetest.register_craft({
	 output = "shrinkgrowrays:growth_ray",
	 recipe = {
	    {"mcl_core:iron_ingot", "mcl_core:diamond", "mesecons:redstone"},
	    {"mcl_core:iron_ingot", "",  ""}
	 }
   })
end
